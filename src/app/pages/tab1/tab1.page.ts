import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../services/news.service';
import { Article } from '../../interfaces/interface';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{

  articles: Article[] = [];

  constructor(private newService: NewsService) {}

  ngOnInit(){
    this.loadNews();
  }

  loadData(event){
    this.loadNews(event);
  }

  loadNews(event?){
    this.newService.getTopHeadLines()
    .subscribe(result => {  
      this.articles.push( ...result.articles);

      if (result.articles.length - 20 < 0){
        event.target.disabled = true;
        event.target.complete();
        return;
      }

      if(event){
        event.target.complete();
      }
    });
  }
}
