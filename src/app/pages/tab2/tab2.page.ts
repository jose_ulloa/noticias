import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../services/news.service';
import { Article } from '../../interfaces/interface';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  categories = ["business","entertainment","general","health","science","sports","technology"];
  articles: Article[] = [];

  constructor(private newService: NewsService){}

  category: string = '';

  ngOnInit(){
    this.loadNews(this.categories[0]);
  }

  changeCategory( event ){
    this.articles = [];    
    this.loadNews(event.detail.value);
  }

  loadData(event){
    this.loadNews(this.category, event);
  }

  loadNews(category: string, event?){
    this.category = category;
    
    this.newService.getTopHeadlinesCategory(category)
    .subscribe(result => {
      this.articles.push( ...result.articles);

      if (result.articles.length - 20 < 0){
        event.target.disabled = true;
        event.target.complete();
        return;
      }

      if(event){
        event.target.complete();
      }
    });
  }
}
