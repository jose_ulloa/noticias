import { Component, OnInit, Input } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Article } from '../../interfaces/interface';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActionSheetController } from '@ionic/angular';
import { LocalDataService } from '../../services/local-data.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss'],
})
export class NewComponent implements OnInit {

  @Input() article: Article;
  @Input() row: number = 0;

  constructor(
    private iab: InAppBrowser, 
    private actionSheetController: ActionSheetController,
    private socialSharing: SocialSharing,
    private localDataService: LocalDataService,
    private toastController: ToastController
  ) { }

  opeNews(){
    const browser = this.iab.create(this.article.url, '_system');
  }

  async showMessage(msg: string){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async launchMenu(){

    let buttonFavorite;
    if ( this.localDataService.isExit(this.article)){
      buttonFavorite = {
        text: 'Delete Favorite',
        icon: 'trash',
        cssClass: 'action-dark',
        handler: () => {
          this.localDataService.deleteFavorite(this.article);
          this.showMessage("It was successfully removed.");
        }
      };
    } 
    else {
      buttonFavorite = {
        text: 'Favorite',
        icon: 'heart',
        cssClass: 'action-dark',
        handler: () => {
          this.localDataService.saveLocalStorage(this.article);
          this.showMessage("It was added successfully.");
        }
      };
    }

    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Share',
          icon: 'share',
          cssClass: 'action-dark',
          handler: () => {
            this.socialSharing.share(
              this.article.title, this.article.source.name,
              '', this.article.url
            );
          }
        }, 
        buttonFavorite, 
        {
          text: 'Cancel',
          icon: 'close',
          cssClass: 'action-dark',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  ngOnInit() {}
}
