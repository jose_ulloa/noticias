import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResultTopHeadLines } from '../interfaces/interface';
import { environment } from '../../environments/environment';

const apiKey = environment.apiKey;
const apiUrl = environment.apiNewURL;

const headers = new HttpHeaders({
  'X-Api-Key': apiKey
});

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private httpClient: HttpClient) { }

  headLinesPage: number = 0;
  currentCategory: string = '';
  categoryPage: number = 0;

  private runGet<T>(query: string){
    query = apiUrl + query;
    return this.httpClient.get<T>(query, { headers });
  }

  getTopHeadLines(){
    this.headLinesPage++;
    this.categoryPage = 0;
    return this.runGet<ResultTopHeadLines>('/top-headlines?country=us&page='+this.headLinesPage);
  }

  getTopHeadlinesCategory( category: string){
    this.headLinesPage = 0;
    if (this.currentCategory === category){
      this.categoryPage++;
    } else {
      this.currentCategory = category;
      this.categoryPage = 1;
    }
    return this.runGet<ResultTopHeadLines>('/top-headlines?country=us&category=' + category + '&page=' + this.categoryPage );
  }
}
