import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Article } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class LocalDataService {

  constructor(private storage: Storage) { 
    this.loadFavorites();
  }

  articles: Article[] = [];

  isExit(article: Article){
    return this.articles.find(
        data => data.title === article.title
      );
  }


  saveLocalStorage(article: Article){
    const exit = this.isExit(article);

    if (!exit){
      this.articles.unshift(article);
      this.storage.set('favorites', this.articles);
    }
  }

  private async loadFavorites(){
   const favorites = await this.storage.get('favorites');
   if(favorites){
    this.articles = favorites;
   }
  }

  deleteFavorite(article: Article){
    this.articles = this.articles.filter( data => data.title !== article.title);
    this.storage.set('favorites', this.articles);
  }
}
